.include "tn45def.inc"
.DEF A  = R16
.DEF AH = R17
.DEF B  = R18
.DEF C  = R20

.ORG $0000

SBI DDRB, 0x4 
;SBI PORTB, 0x3

RESET:
	SBI PORTB,3  
	SBI DDRB, 0

RLOOP:
	LDI A, 0b11000011 	;High bit-enable ADC, 7th- start conversion; lower three-set prescaler
	OUT ADCSRA,A
	LDI A, 0b00000011 	;Select ADC input on PB3
	OUT ADMUX,A

WAIT: 
	IN A,ADCSRA       	;Read ADC status
	ANDI A,0b00010000	;Check ADC flag ADIF
	SBRC A,3
	RJMP WAIT               ;Wait for corversion to finish

CONV:
	IN A, ADCL		;Both ADCL and ADCH need to be read
	IN AH, ADCH
	CPI A, 200              ;Random big number to compare with
	BRSH higher
	CBI PORTB, 0x4		;Set leg 4 low in light
	RJMP RLOOP

higher:
	SBI PORTB, 0x4		;Set leg 4 high in dark 
	RJMP RLOOP
